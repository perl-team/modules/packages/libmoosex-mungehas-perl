libmoosex-mungehas-perl (0.011-3) UNRELEASED; urgency=medium

  * Update standards version to 4.6.1, no changes needed.
  * Update lintian override info format in d/source/lintian-overrides on line
    2-7.
  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 15 Sep 2022 00:59:41 -0000

libmoosex-mungehas-perl (0.011-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 28 Aug 2022 14:59:06 +0100

libmoosex-mungehas-perl (0.011-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ gregor herrmann ]
  * Update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Jonas Smedegaard ]
  * Simplify rules.
    Stop build-depend on devscripts cdbs.
  * Update git-buildpackage config: Filter any .git* file.
  * Update watch file:
    + Bump to file format 4.
    + Track only MetaCPAN URL.
    + Rewrite usage comment.
    + Use substitution strings.
  * Stop build-depend on dh-buildinfo.
  * Relax to (build-)depend unversioned on libscalar-list-utils-perl
    libtest-requires-perl libmoo-perl libmoose-perl libmouse-perl:
    Needed versions satisfied even in oldstable.
  * Declare compliance with Debian Policy 4.3.0.
  * Set Rules-Requires-Root: no.
  * Mark build-dependencies needed only for testsuite as such.
  * Wrap and sort control file.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage of packaging.
    + Extend coverage for main upstream author.
    + Drop Files section for no longer included CONTRIBUTING file.
  * Add lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 21 Feb 2019 21:45:09 +0100

libmoosex-mungehas-perl (0.007-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Update build and runtime dependencies.
    Add "libtype-tiny-perl | libeval-closure-perl" to Depends, extend
    "libtype-tiny-perl" into "libtype-tiny-perl | libeval-closure-perl" in
    Build-Depends. (Closes: #896537)
  * Mark package as autopkgtest-able.
  * Bump debhelper compatibility level to 9.
  * Use HTTPS for a couple of URLs in debian/*.
  * Drop old alternative build dependency on Test::More.
  * Declare compliance with Debian Policy 4.1.4.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Apr 2018 16:50:54 +0200

libmoosex-mungehas-perl (0.007-2) unstable; urgency=medium

  * Update package relations:
    + Fix Tighten (build-)dependency on libscalar-list-utils-perl (add
      missing epoch).

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 25 Feb 2015 12:28:45 +0100

libmoosex-mungehas-perl (0.007-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#779157.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 24 Feb 2015 23:50:35 +0100
